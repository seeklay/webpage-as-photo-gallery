// viewer.js
chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(tabs[0].id, { action: "getImages" }, function (response) {
      const gallery = document.getElementById("gallery");
      response.images.forEach(function (imageSrc) {
        const img = document.createElement("img");
        img.src = imageSrc;
        gallery.appendChild(img);
      });
    });
  });
  