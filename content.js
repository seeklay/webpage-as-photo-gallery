// content.js

const images = document.querySelectorAll('img');
const galleryContainer = document.createElement('div');
galleryContainer.style.position = 'fixed';
galleryContainer.style.top = '0';
galleryContainer.style.left = '0';
galleryContainer.style.width = '100%';
galleryContainer.style.height = '100%';
galleryContainer.style.backgroundColor = 'rgba(0, 0, 0, 0.9)';
galleryContainer.style.display = 'flex';
galleryContainer.style.justifyContent = 'center';
galleryContainer.style.alignItems = 'center';
galleryContainer.style.zIndex = '999999';

const currentImage = document.createElement('img');
currentImage.style.width = '100%';
currentImage.style.maxHeight = '100%';
currentImage.style.objectFit = 'contain';
currentImage.style.cursor = 'pointer';

const thumbnailContainer = document.createElement('div');
thumbnailContainer.style.display = 'flex';
thumbnailContainer.style.gap = '5px';
thumbnailContainer.style.position = 'fixed';
thumbnailContainer.style.bottom = '10%'; // Задаем отступ снизу
thumbnailContainer.style.left = '50%'; // Центрируем по горизонтали
thumbnailContainer.style.transform = 'translateX(-50%)'; // Корректируем для центрирования
thumbnailContainer.style.backgroundColor = 'rgba(0, 0, 0, 0.5)';
thumbnailContainer.style.padding = '10px';

const thumbnails = [];
let bufferIndex = 0;

function updateBuffer() {
  thumbnailContainer.innerHTML = ''; // Очищаем контейнер перед обновлением
  for (let i = bufferIndex; i < bufferIndex + 5; i++) {
    const index = i % images.length;
    const thumbnail = document.createElement('img');
    thumbnail.style.width = '50px';
    thumbnail.style.height = '50px';
    thumbnail.style.objectFit = 'cover';
    thumbnail.style.cursor = 'pointer';
    thumbnail.style.opacity = i === bufferIndex ? '1' : '0.5';
    thumbnail.src = images[index].src;
    thumbnail.addEventListener('click', () => showImage(index));
    thumbnails.push(thumbnail);
    thumbnailContainer.appendChild(thumbnail);
  }
}

updateBuffer();

document.body.innerHTML = ''; // Очищаем страницу
document.body.appendChild(thumbnailContainer); // Переносим блок буфера перед блоком предпросмотра
document.body.appendChild(galleryContainer);

galleryContainer.appendChild(currentImage);

let currentIndex = 0;

images.forEach((image, index) => {
  image.addEventListener('click', () => showImage(index));
});

galleryContainer.addEventListener('click', () => closeGallery());

document.addEventListener('keydown', (event) => {
  if (event.key === 'ArrowLeft') {
    showImage(currentIndex - 1);
  } else if (event.key === 'ArrowRight') {
    showImage(currentIndex + 1);
  } else if (event.key === 'Escape') {
    closeGallery();
  }
});

function showImage(index) {
  if (index >= 0 && index < images.length) {
    currentIndex = index;
    currentImage.src = images[index].src;
    highlightCurrentThumbnail();
    galleryContainer.style.display = 'flex';
  }
}

function closeGallery() {
  galleryContainer.style.display = 'none';
}

function highlightCurrentThumbnail() {
  thumbnails.forEach((thumbnail, index) => {
    thumbnail.style.opacity = index === currentIndex % images.length ? '1' : '0.5';
  });
}

galleryContainer.style.overflow = 'hidden';

// Добавлен обработчик для обновления буфера при прокрутке
window.addEventListener('scroll', () => {
  if (window.scrollY + window.innerHeight >= document.body.scrollHeight) {
    bufferIndex += 5;
    updateBuffer();
  }
});

// Показать первое изображение сразу
showImage(0);
